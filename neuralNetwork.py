import numpy
import time
from keras.models import Sequential
from keras.layers import Dense
from sklearn import preprocessing
from keras.models import load_model

def get_bool(prompt):
    while True:
        try:
           return {"":True,"y":True,"n":False}[input(prompt+"  [y]/n: ").lower()]
        except KeyError:
           print("Invalid input please enter True or False!")

#seed
seed = 7
numpy.random.seed(seed)

print('loading data...')
dataset = numpy.loadtxt("preprocessed-minmax.csv", delimiter=",")


#print('preprocessing data...')
#normal_scaler = pre
# split into input (X) and output (Y) variables
X = dataset[:,0:3]
Y = dataset[:,3]
if(get_bool("Use saved model?")):
    model = load_model('model.h5')
else:
    #create models
    model = Sequential()
    model.add(Dense(12,input_dim=3, init='normal', activation='softplus'))
    model.add(Dense(24, init='normal', activation='softplus'))
    model.add(Dense(8, init='normal', activation='softplus'))
    model.add(Dense(1, init='normal', activation='softplus'))
    model.compile(loss='mean_squared_error',optimizer='adam')

    model.fit(X, Y, nb_epoch=500, batch_size=50)

    model.save(filepath='model.h5')

    scores = model.evaluate(X, Y)
    print()

#print("%s: %.2f%%" % (model.metrics_names[0], scores[0]))
print('generating prediction...')
prediction = model.predict(X, batch_size=32, verbose=0)
prediction_scaled = preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5)).fit_transform(prediction)
#table = numpy.append(X,Y, axis=1)
table = numpy.append(dataset,prediction_scaled, axis=1)
print(table)
timestr = time.strftime("%Y%m%d-%H%M%S")
numpy.savetxt("prediction-%s.csv" % timestr, table, delimiter=",", header="eccentricity,obliquity,longitudePerihelion,prediction", comments='')
print('done!')