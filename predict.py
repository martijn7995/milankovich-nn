import numpy
import time
from keras.models import Sequential
from keras.layers import Dense
from sklearn import preprocessing
from keras.models import load_model

#seed
seed = 7
numpy.random.seed(seed)

print('loading data...')
dataset = numpy.loadtxt("preprocessed-input-minmax.csv", delimiter=",")

model = load_model('model.h5')

#print("%s: %.2f%%" % (model.metrics_names[0], scores[0]))
print('generating prediction...')
prediction = model.predict(dataset, batch_size=50, verbose=0)
prediction_scaled = preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5)).fit_transform(prediction)
#table = numpy.append(X,Y, axis=1)
#table = numpy.append(dataset,prediction_scaled, axis=1)
timestr = time.strftime("%Y%m%d-%H%M%S")
numpy.savetxt("prediction-%s.csv" % timestr, prediction_scaled, delimiter=",", header="prediction", comments='')
print('done!')