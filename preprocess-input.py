import numpy
from sklearn import preprocessing

#time, temperature, eccentricity, obliquity, longitudePerihelion
# vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# eccentricity, obliquity, longitudePerihelion, temperature

DATASET = numpy.loadtxt("milankovitch-input.csv", delimiter=",", skiprows=1, usecols=(1, 2, 3))

print('preparing minmax file')
DATASET_SCALED_MINMAX = preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5)).fit_transform(DATASET)
print('saving minmax file')
numpy.savetxt("preprocessed-input-minmax.csv", DATASET_SCALED_MINMAX, delimiter=",")
