import numpy
from sklearn import preprocessing

#time, temperature, eccentricity, obliquity, longitudePerihelion
# vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# eccentricity, obliquity, longitudePerihelion, temperature

DATASET = numpy.loadtxt("milankovitchAndVostok.csv", delimiter=",", skiprows=1, usecols=(1, 2, 3, 4))

print('preparing minmax file')
DATASET_SCALED_MINMAX = preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5)).fit_transform(DATASET)
print('saving minmax file')
numpy.savetxt("preprocessed-minmax.csv", DATASET_SCALED_MINMAX, delimiter=",")

#print('preparing normal file')
#DATASET_SCALED_NORMAL = preprocessing.scale(DATASET)
#print('saving normal file')
#numpy.savetxt("preprocessed-normal.csv", DATASET_SCALED_NORMAL, delimiter=",")
